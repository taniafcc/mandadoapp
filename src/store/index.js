import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const IN_STOCK = true;
const TO_BUY = false;

export default new Vuex.Store({
  state: {
    products: [],
    product: {
      id: 0,
      description: '',
      status: TO_BUY,
      addedAt: '', 
    },
    categories: [
      {
        title: 'En existencia',
        value: IN_STOCK
      }, 
      {
        title: 'Por comprar',
        value: TO_BUY
      }
    ],
    filteredProducts: [],
    productsInStock: [],
    productsToBuy: [],
  },
  mutations: {
    loadProducts(state, list) {
      state.products = list
    },
    setProducts (state, product) {
      state.products.push(product)
      localStorage.setItem('products', JSON.stringify(state.products))
    },
    setFilteredProducts (state, products) {
      state.filteredProducts = products
    },
    deleteProduct (state, id) {
      state.products = state.products.filter(product => product.id != id)
      localStorage.setItem('products', JSON.stringify(state.products))
    },
    updateProduct (state, product) {
      state.products = state.products.map(item => {
        return item.id === product.id ? product : item
      })
      localStorage.setItem('products', JSON.stringify(state.products))
    }
  },
  actions: {
    setProducts ({ commit }, product) {
      commit('setProducts', product)
    },
    deleteProduct ({ commit, state }, id) {
      commit('deleteProduct', id)
    },
    updateProduct ({ commit }, product) {
      commit('updateProduct', product)
    },
    filterByStatus ({ commit, state }, status) {
      const filter = state.products.filter(product => product.status === status)
      commit('setFilteredProducts', filter)
    },
    loadLocalStorage ({ commit }) {
      if (localStorage.getItem('products')) {
        const products = JSON.parse(localStorage.getItem('products'))
        commit('loadProducts', products)
        return
      }
      localStorage.setItem('products', JSON.stringify([]))
    }
  },
  modules: {

  }
})
