# MandadoApp
Ejercicio de programacion con vue.

Lista de productos del mandado, se dividen en dos
categorias: En existencia y Por comprar

---
[Demo](https://reto-programacion.netlify.app) 

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
